"""This is the module for encryption and decryption algorithms."""

from . import elementary


def rsa_generate_prime(security_bits, public_exp=65537):
    """Return a prime for the RSA encryption with length given by security_bits.

    In order to keep the encryption, i.e. the binary exponentiation of a^V, fast,
    we want V to be both short and contain few ones in its binary representation."""

    # in order to have at least two suitable primes, we need at least 5 security bits
    security_bits = max(security_bits, 5)

    # probability of not getting a prime is at most 1 to 100,000,000
    certainty_level = 30
    # attempts should not matter since we repeat the prime generation until
    # a prime is found
    attempts = 50
    # the length of the prime should be at least security_bits
    start = 2 ** (security_bits - 1)
    # the length of the prime should be at most security_bits
    end = 2**security_bits

    while True:
        prime = elementary.generate_prime(start, end, attempts, certainty_level)
        if prime is not None and (prime - 1) % public_exp != 0:
            return prime


def rsa_generate_prime_pair(security_bits, public_exp=65537):
    """Return a prime for the RSA encryption with length given by security_bits.

    In order to keep the encryption, i.e. the binary exponentiation of a^V, fast,
    we want V to be both short and contain few ones in its binary representation."""

    first_prime = rsa_generate_prime(security_bits, public_exp)

    while True:
        second_prime = rsa_generate_prime(security_bits, public_exp)
        if first_prime != second_prime:
            break

    return first_prime, second_prime


def rsa_generate_keys(security_bits, public_exp=65537):
    """Return a key pair for RSA encryption

    The modulus of the key is the product of two primes with length given by
    security_bits.

    In order to keep the encryption, i.e. the binary exponentiation of a^V, fast,
    we want V to be both short and contain few ones in its binary representation."""

    first_prime, second_prime = rsa_generate_prime_pair(security_bits, public_exp)

    mod = first_prime * second_prime
    phi = (first_prime - 1) * (second_prime - 1)

    _, private_exp_mod = elementary.bezout(phi, public_exp)
    private_exp = private_exp_mod % phi

    return (private_exp, mod), (public_exp, mod)


def rsa_encrypt(msg, public_key):
    """Return the RSA-encrypted message"""
    public_exp, mod = public_key

    return elementary.modular_exponentiation(msg, public_exp, mod)


def rsa_decrypt(msg, private_key):
    """Return the RSA-encrypted message"""
    private_exp, mod = private_key

    return elementary.modular_exponentiation(msg, private_exp, mod)


def rsa_sign(msg, private_key):
    """Return the message and the signed message"""
    private_exp, mod = private_key

    return msg, elementary.modular_exponentiation(msg, private_exp, mod)


def rsa_verify(msg, msg_signed, public_key):
    """Check the signed message"""
    public_exp, mod = public_key

    return msg == elementary.modular_exponentiation(msg_signed, public_exp, mod)
