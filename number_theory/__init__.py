"""In this project, we implement algorithms related to number theory.

The algorithms are for teaching purposes only.

The algorithms may not behave correctly in all edge cases
and the encryptions are not designed to offer real security.
"""
