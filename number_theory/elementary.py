"""This is the module for elementary number theoretic algorithms."""

import random


def root_floor(radicant, degree=2):
    """Return the largest integer that is at most degree-th root of the radicant"""

    if radicant == 0:
        return 0

    root_max = 1

    while root_max**degree < radicant:
        root_max = root_max * 2

    root_min = root_max // 2

    while root_min < root_max:
        root_tmp = (root_min + root_max) // 2 + 1
        if root_tmp**degree <= radicant:
            root_min = root_tmp
        else:
            root_max = root_tmp - 1

    return root_min


def root_ceil(radicant, degree=2):
    """Return the smallest integer that is at least degree-th root of the radicant"""

    root_tmp = root_floor(radicant, degree)

    if root_tmp**degree == radicant:
        return root_tmp
    return root_tmp + 1


def is_integer_root(radicant, degree=2):
    """Return True if degree-th root of the radicant is an integer"""

    return root_floor(radicant, degree) == root_ceil(radicant, degree)


def binary_exponentiation(base, exp, mod):
    """Return (base ** exp) % mod

    This function uses binary exponentiation"""

    if exp == 0:
        return 1

    if exp % 2 == 0:
        base = (base**2) % mod
        exp = exp / 2
        power = binary_exponentiation(base, exp, mod)
        return power

    # The exponent is odd
    factor = base
    base = (base**2) % mod
    exp = (exp - 1) / 2
    power = (factor * binary_exponentiation(base, exp, mod)) % mod
    return power


def modular_exponentiation(base, exp, mod):
    """Return (base ** exp) % mod"""
    return binary_exponentiation(base, exp, mod)


def euclidean_algorithm(first_num, second_num):
    """Return the greatest common divisor of two integers

    This function uses the Euclidean algorithm"""

    if second_num == 0:
        return first_num

    return euclidean_algorithm(second_num, first_num % second_num)


def greatest_common_divisor(first_num, second_num):
    """Return the greatest common divisor of two integers"""

    return euclidean_algorithm(first_num, second_num)


def extended_euclidean_algorithm(first_num, second_num):
    """Return the greatest common divisor of two integers and a solution
    for gcd = first_factor * first_num + second_factor * second_num

    This function uses the Euclidean algorithm"""

    if second_num == 0:
        return first_num, 1, 0

    quotient = first_num // second_num
    # This is equal to first_num % second_num
    residue = first_num - (quotient) * second_num

    (
        gcd,
        first_subfactor,
        second_subfactor,
    ) = extended_euclidean_algorithm(second_num, residue)

    first_factor = second_subfactor
    second_factor = first_subfactor - second_subfactor * quotient

    return gcd, first_factor, second_factor


def bezout(first_num, second_num):
    """Return a solution for
    gcd = first_factor * first_num + second_factor * second_num"""

    _, first_factor, second_factor = extended_euclidean_algorithm(first_num, second_num)
    return first_factor, second_factor


def chinese_remainder_theorem(first_res, first_mod, second_res, second_mod):
    """Return a solution for the simultaneous congruence

    This function uses the algorithm for the Chinese remainder theorem which
    uses the extended Euclidean algorithm"""

    _, first_factor, second_factor = extended_euclidean_algorithm(first_mod, second_mod)

    simultaneous_mod = first_mod * second_mod
    simultaneous_res = (
        second_res * first_factor * first_mod + first_res * second_factor * second_mod
    ) % simultaneous_mod

    return simultaneous_res, simultaneous_mod


def simultaneous_congruence(first_res, first_mod, second_res, second_mod):
    """Return a solution for the simultaneous congruence"""
    return chinese_remainder_theorem(first_res, first_mod, second_res, second_mod)


def split_odd(number):
    """Split a number int an odd number and a power of two"""

    even_power = 0
    while number % 2 == 0:
        even_power = even_power + 1
        number = number / 2
    return number, even_power


def primality_test_miller_rabin(candidate, certainty_level):
    """Test the primality of the candidate using the miller rabin primality test

    If candidate is a random integer, the probability that the result is
    false positive is at most 1 / (2 ** certainty_level)"""

    # 0 and 1 are not prime
    if candidate in [0, 1]:
        return False
    # Even prime
    if candidate == 2:
        return True
    # Even non-prime
    if candidate % 2 == 0:
        return False

    # Even if we account for the rarity of primes,
    # the probability for false positives is less than 1/4
    # See "Further investigations with the strong probable prime test"
    # by Ronald Joseph Burthe, Jr.
    # https://www.ams.org/journals/mcom/1996-65-213/S0025-5718-96-00695-3/S0025-5718-96-00695-3.pdf
    attempts = (1 + certainty_level) // 2
    odd, even_power = split_odd(candidate - 1)

    for _ in range(attempts):
        witness = random.randrange(2, candidate)
        witness = modular_exponentiation(witness, odd, candidate)

        if witness == candidate - 1:
            continue
        if witness == 1:
            continue

        for _ in range(even_power - 1):
            witness = (witness**2) % candidate
            if witness == candidate - 1:
                break
            # Miller-Rabin non-prime
            if witness == 1:
                return False

        if witness == candidate - 1:
            continue
        # Fermat non-prime
        return False

    # Maybe prime
    return True


def primality_test_fast_probabilistic(candidate, certainty_level):
    """Test the primality of the candidate using a fast, probabilistic primality test

    If candidate is a random integer, the probability that the result is
    false positive is at most 1 / (2 ** certainty_level)"""
    return primality_test_miller_rabin(candidate, certainty_level)


def primality_test(candidate, certainty_level):
    """Test the primality of the candidate

    If candidate is a random integer, the probability that the result is
    false positive is at most 1 / (2 ** certainty_level)"""
    return primality_test_fast_probabilistic(candidate, certainty_level)


def generate_prime(start, end, attempts, certainty_level):
    """Try to find a prime between start (included) and end (not included)

    Fails if none of the attempted candidates is prime.
    If successful and end - start is sufficiently large, the probability that
    the returned value is prime is at least 1 / (2 ** certainty_level)"""

    for _ in range(attempts):
        candidate = random.randrange(start, end)
        if primality_test_miller_rabin(candidate, certainty_level):
            return candidate

    return None


def factorization_trial_division(number, attempts):
    """Return the smallest nontrivial divisor of number or None
    if the smallest divisor is at least the number of attempts"""

    # The smallest nontrivial divisor is at most the root of number.
    if attempts > root_floor(number):
        attempts = root_floor(number) + 1

    for i in range(2, attempts):
        if number % i == 0:
            return i

    return None


def factorization_fermat_method(number, attempts):
    """Return a nontrivial divisor of number or None if none was found"""

    if number <= 2:
        return None
    if number % 2 == 0:
        return 2

    base_minuend_root = root_ceil(number)

    for i in range(attempts):
        minuend_root = base_minuend_root + i
        subtrahend = minuend_root**2 - number

        if is_integer_root(subtrahend):
            # 1 is not nontrivial
            if minuend_root - root_floor(subtrahend) > 1:
                return minuend_root - root_floor(subtrahend)
            return None

    return None


def factorization(number, attempts):
    """Return a nontrivial divisor of number or None if none was found"""

    factor = factorization_trial_division(number, attempts)
    if factor is None:
        factor = factorization_fermat_method(number, attempts)

    return factor
