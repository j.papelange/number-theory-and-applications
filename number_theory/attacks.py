"""This is the module for attacks on the cryptosystems."""

import random

from . import elementary
from . import encryption


def rsa_generate_key_with_known_prime(first_prime, public_key):
    """Return private_key if a factor of the modulus is known"""

    public_exp, mod = public_key

    if (
        first_prime is None
        or first_prime == 1
        or first_prime == mod
        or mod % first_prime != 0
    ):
        return None

    second_prime = mod // first_prime
    phi = (first_prime - 1) * (second_prime - 1)
    _, private_exp_mod = elementary.bezout(phi, public_exp)
    private_exp = private_exp_mod % phi

    return (private_exp, mod)


def rsa_root_attack(msg_encrypted, public_key, attempts):
    """Return the decrypted msg if the decrypted message is of the form
    +-((i * modulus) + r) with 0 <= r < modulus and i < attempts"""

    public_exponent, public_modulus = public_key

    for i in range(attempts):
        if elementary.is_integer_root(
            (i * public_modulus) + msg_encrypted, public_exponent
        ):
            return elementary.root_floor(
                (i * public_modulus) + msg_encrypted, public_exponent
            )

    for i in range(attempts):
        if elementary.is_integer_root(
            (i * public_modulus) + (public_modulus - msg_encrypted), public_exponent
        ):
            return public_modulus - elementary.root_floor(
                (i * public_modulus) + (public_modulus - msg_encrypted), public_exponent
            )

    return None


def rsa_factorization_attack(public_key, attempts):
    """Try to return the private key by factorization of the modulus"""

    _, mod = public_key

    first_prime = elementary.factorization(mod, attempts)

    return rsa_generate_key_with_known_prime(first_prime, public_key)


def rsa_private_key_attack(public_key, attempts, certainty_level):
    """Try to return the private key by guessing it directly"""

    _, mod = public_key

    for private_exp in range(attempts):
        private_key = (private_exp, mod)
        key_found = True
        for _ in range(certainty_level):
            msg = random.randrange(0, mod)
            msg_encrypted = encryption.rsa_encrypt(msg, public_key)
            msg_decrypted = encryption.rsa_decrypt(msg_encrypted, private_key)
            if msg != msg_decrypted:
                key_found = False
                break
        if key_found:
            return private_key

    return None


def rsa_gcd_attack(msg_encrypted, public_key):
    """Return the private key if the modulus and the message have a common divisor"""

    _, mod = public_key

    first_prime = elementary.greatest_common_divisor(msg_encrypted, mod)

    return rsa_generate_key_with_known_prime(first_prime, public_key)
