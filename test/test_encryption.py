"""This tests the module for encryption and decryption algorithms."""

import random
from collections import namedtuple

from number_theory import elementary
from number_theory import encryption


def test_rsa_key_generation():
    """Test RSA-key generation"""

    key_generating_functions = [encryption.rsa_generate_keys]

    Test_Case = namedtuple("Test_Case", "security_bits public_exp")
    test_cases = [Test_Case(5, 3), Test_Case(10, 17), Test_Case(20, 65537)]

    for function in key_generating_functions:
        for case in test_cases:
            # The prime generation uses random numbers
            random.seed(0)

            private_key, public_key = function(case.security_bits, case.public_exp)
            private_exp, private_mod = private_key
            public_exp, public_mod = public_key

            assert private_mod == public_mod
            assert private_mod >= (2 ** (case.security_bits - 1)) ** 2
            assert private_mod < (2**case.security_bits) ** 2

            random.seed(0)
            for _ in range(10):
                msg = random.randrange(0, private_mod)
                msg_encrypted = elementary.modular_exponentiation(
                    msg, public_exp, public_mod
                )
                msg_decrypted = elementary.modular_exponentiation(
                    msg_encrypted, private_exp, private_mod
                )
                assert msg == msg_decrypted


def test_rsa_encryption_decryption():
    """Test RSA encryption and decryption"""

    key_generating_functions = [encryption.rsa_generate_keys]

    Test_Case = namedtuple("Test_Case", "security_bits public_exp")
    test_cases = [Test_Case(5, 3), Test_Case(10, 17), Test_Case(20, 65537)]

    for function in key_generating_functions:
        for case in test_cases:
            # The prime generation uses random numbers
            random.seed(0)

            private_key, public_key = function(case.security_bits, case.public_exp)
            _, private_mod = private_key

            random.seed(0)
            for _ in range(10):
                msg = random.randrange(0, private_mod)
                msg_encrypted = encryption.rsa_encrypt(msg, public_key)
                msg_decrypted = encryption.rsa_decrypt(msg_encrypted, private_key)
                assert msg == msg_decrypted


def test_rsa_signing_verification():
    """Test RSA signing and verification"""

    key_generating_functions = [encryption.rsa_generate_keys]

    Test_Case = namedtuple("Test_Case", "security_bits public_exp")
    test_cases = [Test_Case(5, 3), Test_Case(10, 17), Test_Case(20, 65537)]

    for function in key_generating_functions:
        for case in test_cases:
            # The prime generation uses random numbers
            random.seed(0)

            private_key, public_key = function(case.security_bits, case.public_exp)
            _, private_mod = private_key

            random.seed(0)
            for _ in range(10):
                msg = random.randrange(0, private_mod)
                msg_copy, msg_signed = encryption.rsa_sign(msg, private_key)
                assert msg == msg_copy
                assert encryption.rsa_verify(msg_copy, msg_signed, public_key)
