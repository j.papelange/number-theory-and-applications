"""This tests the module for encryption and decryption algorithms."""

import random
from collections import namedtuple

from number_theory import attacks
from number_theory import encryption


def test_rsa_root_attack():
    """Test RSA root attack"""

    attempts = 20

    attack_functions = [attacks.rsa_root_attack]

    Test_Case = namedtuple("Test_Case", "msg public_key")
    test_cases = [
        Test_Case(5, (3, 1_727_782_939)),
        Test_Case(1200, (3, 1_727_782_939)),
        Test_Case(1_727_781_739, (3, 1_727_782_939)),
    ]

    for function in attack_functions:
        for case in test_cases:
            msg_encrypted = encryption.rsa_encrypt(case.msg, case.public_key)
            assert case.msg == function(msg_encrypted, case.public_key, attempts)

    Test_Case = namedtuple("Test_Case", "msg public_key")
    test_cases = [
        Test_Case(727_782_939, (3, 1_727_782_939)),
    ]

    for function in attack_functions:
        for case in test_cases:
            msg_encrypted = encryption.rsa_encrypt(case.msg, case.public_key)
            assert function(msg_encrypted, case.public_key, attempts) is None


def test_rsa_factorization_attack():
    """Test RSA factorization attack"""

    attempts = 20

    attack_functions = [attacks.rsa_factorization_attack]

    Test_Case = namedtuple("Test_Case", "public_key, private_key")
    test_cases = [
        Test_Case((3, 449_485), (239_723, 449_485)),
        Test_Case((3, 1_770_473_893), (1_180_259_827, 1_770_473_893)),
        # 991 * 99_623 = 98_726_393 cannot be factorized in just 20 attempts
        Test_Case((3, 98_726_393), None),
    ]

    for function in attack_functions:
        for case in test_cases:
            assert case.private_key == function(case.public_key, attempts)


def test_rsa_private_key_attack():
    """Test RSA private key attack"""

    attempts = 20
    certainty_level = 20

    attack_functions = [attacks.rsa_private_key_attack]

    Test_Case = namedtuple("Test_Case", "public_key, private_key")
    test_cases = [
        Test_Case((1_482_460_227, 2_223_784_657), (3, 2_223_784_657)),
        # 991 * 99_623 = 98_726_393 cannot be factorized in just 20 attempts
        Test_Case((3, 98_726_393), None),
    ]

    for function in attack_functions:
        for case in test_cases:
            random.seed(0)
            assert case.private_key == function(
                case.public_key, attempts, certainty_level
            )


def test_rsa_gcd_attack():
    """Test RSA private key attack"""

    attack_functions = [attacks.rsa_gcd_attack]

    Test_Case = namedtuple("Test_Case", "msg public_key, private_key")
    test_cases = [
        Test_Case(243_709_603, (3, 1_241_611_573), (827_694_067, 1_241_611_573)),
    ]

    for function in attack_functions:
        for case in test_cases:
            random.seed(0)
            assert case.private_key == function(case.msg, case.public_key)
