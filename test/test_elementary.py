"""This tests the module for elementary number theoretic algorithms."""

import random
from collections import namedtuple

from number_theory import elementary


def test_root():
    """Test root functions"""
    root_floor_functions = [elementary.root_floor]

    Test_Case = namedtuple("Test_Case", "radicant degree result")
    test_cases = [
        Test_Case(0, 2, 0),
        Test_Case(1, 2, 1),
        Test_Case(9, 2, 3),
        Test_Case(24, 2, 4),
        Test_Case(25, 2, 5),
        Test_Case(26, 2, 5),
        Test_Case(0, 5, 0),
        Test_Case(1, 5, 1),
        Test_Case(31, 5, 1),
        Test_Case(32, 5, 2),
        Test_Case(33, 5, 2),
        Test_Case(1023, 5, 3),
        Test_Case(1024, 5, 4),
        Test_Case(1025, 5, 4),
    ]

    for function in root_floor_functions:
        for case in test_cases:
            assert function(case.radicant, case.degree) == case.result

    root_ceil_functions = [elementary.root_ceil]

    Test_Case = namedtuple("Test_Case", "radicant degree result")
    test_cases = [
        Test_Case(0, 2, 0),
        Test_Case(1, 2, 1),
        Test_Case(9, 2, 3),
        Test_Case(24, 2, 5),
        Test_Case(25, 2, 5),
        Test_Case(26, 2, 6),
        Test_Case(0, 5, 0),
        Test_Case(1, 5, 1),
        Test_Case(31, 5, 2),
        Test_Case(32, 5, 2),
        Test_Case(33, 5, 3),
        Test_Case(1023, 5, 4),
        Test_Case(1024, 5, 4),
        Test_Case(1025, 5, 5),
    ]

    for function in root_ceil_functions:
        for case in test_cases:
            assert function(case.radicant, case.degree) == case.result


def test_modular_exponentiation():
    """Test modular exponentiation"""

    modular_exponentiation_functions = [
        elementary.modular_exponentiation,
        elementary.binary_exponentiation,
    ]

    Test_Case = namedtuple("Test_Case", "base exp mod result")
    test_cases = [
        Test_Case(0, 4, 11, 0),
        Test_Case(4, 0, 7, 1),
        Test_Case(3, 0, 21, 1),
        Test_Case(7, 18, 19, 1),
        Test_Case(4, 23, 23, 4),
        Test_Case(4, 250, 29, 20),
    ]

    for function in modular_exponentiation_functions:
        for case in test_cases:
            assert function(case.base, case.exp, case.mod) == case.result


def test_greatest_common_divisor():
    """Test the algorithms for the greatest common divisor"""

    greatest_common_divisor_functions = [
        elementary.greatest_common_divisor,
        elementary.euclidean_algorithm,
        lambda first_num, second_num: elementary.extended_euclidean_algorithm(
            first_num, second_num
        )[0],
    ]

    Test_Case = namedtuple("Test_Case", "first_num, second_num result")
    test_cases = [
        Test_Case(0, 5, 5),
        Test_Case(7, 0, 7),
        Test_Case(1, 8, 1),
        Test_Case(6, 1, 1),
        Test_Case(13, 13, 13),
        Test_Case(89, 144, 1),
        Test_Case(1398, 2262, 6),
    ]

    for function in greatest_common_divisor_functions:
        for case in test_cases:
            assert function(case.first_num, case.second_num) == case.result


def test_bezout():
    """Test the algorithms for Bezout's identity"""

    # elementary.extended_euclidean_algorithm
    bezout_functions = [
        elementary.bezout,
        lambda first_num, second_num: elementary.extended_euclidean_algorithm(
            first_num, second_num
        )[1:],
    ]

    Test_Case = namedtuple("Test_Case", "first_num, second_num")
    test_cases = [
        Test_Case(0, 5),
        Test_Case(7, 0),
        Test_Case(1, 8),
        Test_Case(6, 1),
        Test_Case(13, 13),
        Test_Case(89, 144),
        Test_Case(1398, 2262),
    ]

    for function in bezout_functions:
        for case in test_cases:
            gcd = elementary.greatest_common_divisor(case.first_num, case.second_num)
            first_factor, second_factor = function(case.first_num, case.second_num)
            assert (
                gcd == first_factor * case.first_num + second_factor * case.second_num
            )


def test_simultaneous_congruence():
    """Test simultaneous congruence algorithms"""

    simultaneous_congruence_functions = [
        elementary.simultaneous_congruence,
        elementary.chinese_remainder_theorem,
    ]

    Test_Case = namedtuple("Test_Case", "first_res, first_mod, second_res, second_mod")
    test_cases = [
        Test_Case(0, 7, 4, 23),
        Test_Case(6, 20, 0, 21),
        Test_Case(0, 13, 0, 11),
        Test_Case(6, 26, 9, 27),
    ]

    for function in simultaneous_congruence_functions:
        for case in test_cases:
            simultaneous_res, simultaneous_mod = function(
                case.first_res, case.first_mod, case.second_res, case.second_mod
            )
            assert (
                simultaneous_mod
                == case.first_mod
                * case.second_mod
                / elementary.greatest_common_divisor(case.first_mod, case.second_mod)
            )
            assert simultaneous_res % case.first_mod == case.first_res
            assert simultaneous_res % case.second_mod == case.second_res


def test_primality_check():
    """Test primality testing algorithms"""

    certainty_level = 10

    # primality test algorithms
    primality_test_functions = [
        elementary.primality_test,
        elementary.primality_test_fast_probabilistic,
        elementary.primality_test_miller_rabin,
    ]

    Test_Case = namedtuple("Test_Case", "candidate, is_prime")
    test_cases = [
        Test_Case(0, False),
        Test_Case(1, False),
        Test_Case(2, True),
        Test_Case(230, False),  # 2*5*23
        Test_Case(231, False),  # 3*7*11
        Test_Case(373, True),
        Test_Case(15089, False),  # 15089 = 79*191
        Test_Case(15841, False),  # 15841 = 7*31*73, Carmichael
        Test_Case(53467, False),  # 53467 = 127*421
        Test_Case(62745, False),  # 62745 = 3*5*47*89 Carmichael
        Test_Case(78563, False),  # 78563 = 251*313
    ]

    for function in primality_test_functions:
        for case in test_cases:
            # Some primality tests use random numbers
            random.seed(0)
            assert function(case.candidate, certainty_level) == case.is_prime


def test_prime_generation():
    """Test prime generation algorithms"""

    attempts = 1000
    certainty_level = 10

    # primality test algorithms
    prime_generating_functions = [elementary.generate_prime]

    Test_Case = namedtuple("Test_Case", "start, end, prime")
    test_cases = [
        Test_Case(2, 3, 2),
        Test_Case(8, 12, 11),
        Test_Case(13, 17, 13),
        Test_Case(1328, 1361, None),
    ]

    for function in prime_generating_functions:
        for case in test_cases:
            # The prime generation uses random numbers
            random.seed(0)
            assert (
                function(case.start, case.end, attempts, certainty_level) == case.prime
            )


def test_factorization():
    """Test factorization algorithms"""

    attempts = 100

    factorization_functions = [
        elementary.factorization_trial_division,
        elementary.factorization_fermat_method,
        elementary.factorization,
    ]

    # With nontrivial divisors
    Test_Case = namedtuple("Test_Case", "number")
    test_cases = [
        Test_Case(81),
        Test_Case(183),
        Test_Case(323),
        Test_Case(1024),
        Test_Case(15015),
    ]

    for function in factorization_functions:
        for case in test_cases:
            factor = function(case.number, attempts)
            assert function(case.number, attempts) is not None
            assert factor > 1
            assert factor < case.number
            assert case.number % factor == 0

    # Without nontrivial divisors
    Test_Case = namedtuple("Test_Case", "number")
    test_cases = [
        Test_Case(0),
        Test_Case(1),
        Test_Case(2),
        Test_Case(3),
        Test_Case(11),
        Test_Case(61),
        Test_Case(99991),
    ]

    for function in factorization_functions:
        for case in test_cases:
            assert function(case.number, attempts) is None
