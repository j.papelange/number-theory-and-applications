# Number Theory and Applications

In this project, we implement algorithms related to number theory.

The algorithms are implemented for teaching and learning purposes. The algorithms may not behave correctly in all edge cases and the encryptions are not designed to offer real security.

## Contributing
Since this project is primarily for teaching and learning purposes, it would be nice to optionally output calculations.
If anyone reads this who has worked with logging before and/or has strong opinions on how the calculations should be displayed/logged, I would be happy to read your suggestions.

## License
This project is licensed under the MIT License.
